from rule import Rule


class Graph:
    def __init__(self, facts=[], rules=[]):
        self.facts = facts
        self.rules = rules

    def load_from_file(self, filename):
        with open(filename) as f:
            lines = f.readlines()
        lines = [l.strip() for l in lines]
        self.facts = lines.pop(0).split(' ')
        while lines:
            rule = lines.pop(0)
            parsed_rule = rule.split('=>')
            self.rules.append(Rule(parsed_rule[1].strip(), parsed_rule[0].strip().split(' ')))

    def search_target(self, inputs, target):
        print()
        print('===================')
        print('Search from target:')
        print('===================')
        open_nodes = [target]
        open_rules = []
        closed_nodes = []
        closed_rules = []
        self.print_current_state(open_nodes, open_rules, closed_nodes, closed_rules)

        free_rules = [str(i) for i in range(len(self.rules))]
        iteration_number = 1
        while True:
            print('Iteration: ' + str(iteration_number))
            current_node = open_nodes[-1]
            is_rule_added = False

            i = 0
            while i < len(free_rules) and not is_rule_added:
                if self.rules[int(free_rules[i])].parent == current_node:
                    open_rules.append(free_rules[i])
                    free_rules.pop(i)
                    is_rule_added = True
                i += 1

            self.print_current_state(open_nodes, open_rules, closed_nodes, closed_rules)
            if is_rule_added:
                nodes_from_rule = self.rules[int(open_rules[-1])].children
                for node in nodes_from_rule:
                    open_nodes.append(node)
                self.print_current_state(open_nodes, open_rules, closed_nodes, closed_rules)
                while self.is_open_nodes_reducible(inputs, open_nodes, closed_rules):
                    resolve_node = open_nodes.pop()
                    closed_nodes.append(resolve_node)
                    self.print_current_state(open_nodes, open_rules, closed_nodes, closed_rules)
                    if open_rules:
                        nodes_from_rule = self.rules[int(open_rules[-1])].children
                        if set(nodes_from_rule).issubset(set(closed_nodes)):
                            resolve_rule = open_rules.pop()
                            closed_rules.append(resolve_rule)
                            self.print_current_state(open_nodes, open_rules, closed_nodes, closed_rules)
                if target in closed_nodes:
                    return True
            else:
                open_nodes.pop()
                self.print_current_state(open_nodes, open_rules, closed_nodes, closed_rules)
                if open_nodes:
                    nodes_from_rule = self.rules[int(open_rules[-1])].children
                    if not set(open_nodes).intersection(set(nodes_from_rule)):
                        open_rules.pop()
                        self.print_current_state(open_nodes, open_rules, closed_nodes, closed_rules)
            if not open_nodes:
                return False
            iteration_number += 1

    def is_open_nodes_reducible(self, inputs, open_nodes, closed_rules):
        return len(open_nodes) > 0 and (open_nodes[-1] in inputs or len(closed_rules) > 0 and self.rules[int(closed_rules[-1])].parent == open_nodes[-1])

    def print_current_state(self, open_nodes, open_rules, closed_nodes, closed_rules):
        print('stack of open nodes: ', end='')
        for node in open_nodes[::-1]:
            print(node + ' ', end='')
        print()

        print('stack of open rules: ', end='')
        for rule in open_rules[::-1]:
            print(str(int(rule) + 100) + ' ', end='')
        print()

        print('stack of closed nodes: ', end='')
        for node in closed_nodes[::-1]:
            print(node + ' ', end='')
        print()

        print('stack of closed rules: ', end='')
        for rule in closed_rules[::-1]:
            print(str(int(rule) + 100) + ' ', end='')
        print()
        print()



