from graph import Graph


def main():
    graph = Graph()
    graph.load_from_file('test_data')
    inputs = input('Input facts: ').split(' ')
    target = input('Input target fact: ')

    is_reachable = graph.search_target(inputs, target)
    if is_reachable:
        print('Target fact is reachable')
    else:
        print('target fact is not reachable')


main()